﻿using IdentityModel.Client;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace IS4
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Ожидание запуска сервисов...");
            Console.WriteLine();

            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, error) => true;

            Console.WriteLine("Выполенение авторизованного запроса без токена:");
            await RequestCall(httpClientHandler);        

            var httpClient = new HttpClient(httpClientHandler);

            var disco = await httpClient.GetDiscoveryDocumentAsync("https://localhost:5001");
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                Console.ReadKey();
                return;
            }

            var tokenClient = new TokenClient(httpClient, new TokenClientOptions()
            {
                Address = disco.TokenEndpoint,
                ClientId = "m2m.client",
                ClientSecret = "511536EF-F270-4058-80CA-1C89C192F69A"
            });

            var userName = "artemastafyev";
            var userPassword = "bas15674re";

            Console.WriteLine("Получение токена для пользователя: " + userName);
            var tokenResponse = await tokenClient.RequestPasswordTokenAsync(userName, userPassword, "scope1");
            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Токен получен:");
            Console.WriteLine(tokenResponse.AccessToken);
            Console.WriteLine("");

            var accessToken = tokenResponse.AccessToken;

            Console.WriteLine("Выполенение авторизованного запроса с полученным токеном:");
            await RequestCall(httpClientHandler, accessToken);

            Console.ReadKey();
        }

        private static async Task RequestCall(HttpClientHandler httpClientHandler, string accessToken = null)
        {
            var apiClient = new HttpClient(httpClientHandler);
            
            if (accessToken != null)
                apiClient.SetBearerToken(accessToken);
          
            var response = await apiClient.GetAsync("https://localhost:44354/WeatherForecast");
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Авторизованный запрос выполнен успешно.");
                Console.WriteLine(await response.Content.ReadAsStringAsync());
            }
            else
            {
                Console.WriteLine("Авторизованный запрос отклонен.");
            }

            Console.WriteLine();
        }
    }
}
